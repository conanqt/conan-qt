from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout
from conan.tools.scm import Git

class ConanQt(ConanFile):
    name="conanQt"
    
    settings="os","compiler","build_type","arch"
    generators = "VirtualBuildEnv"
    
    def configure(self):
        self.options["qt"].shared = True
        self.options["qt"].qtdeclarative = True
        self.options["qt"].qttranslations = True
        self.options["qt"].qtimageformats = True
        self.options["qt"].qtlanguageserver = True
        self.options["qt"].qtshadertools = True
        self.options["qt"].qtsvg = True
        

    def requirements(self):
        self.requires("qt/6.5.3")
        self.requires("libpng/1.6.42")
        self.requires("openssl/3.2.1")
        self.requires("zlib/1.3.1")
        self.requires("xkbcommon/1.5.0")
        self.requires("openal-soft/1.22.2")
    
    def build_requirements(self):
        self.tool_requires("ninja/1.11.1")
        self.tool_requires("cmake/3.28.1")
        
    