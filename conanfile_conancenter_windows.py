from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout
from conan.tools.scm import Git

class ConanQt(ConanFile):
    name="conanQt"
    
    settings="os","compiler","build_type","arch"
    generators = "VirtualBuildEnv"
    
    def configure(self):
        self.options["qt"].shared = True
        self.options["qt"].qtdeclarative = True
        self.options["qt"].qttranslations = True
        self.options["qt"].qtimageformats = True
        self.options["qt"].qtlanguageserver = True
        self.options["qt"].qtshadertools = True
        self.options["qt"].qtsvg = True

    def requirements(self):
        self.requires("qt/6.5.3")
        self.requires("brotli/1.1.0")
        self.requires("bzip2/1.0.8")
        self.requires("double-conversion/3.3.0")
        self.requires("freetype/2.13.2")
        self.requires("glib/2.78.1")
        self.requires("harfbuzz/8.3.0")
        self.requires("libffi/3.4.4")
        self.requires("libgettext/0.22")
        self.requires("libiconv/1.17")
        self.requires("libpng/1.6.42")
        self.requires("libpq/15.4")
        self.requires("md4c/0.4.8")
        self.requires("openssl/3.2.1")
        self.requires("pcre2/10.42")
        self.requires("sqlite3/3.45.0")
        self.requires("zlib/1.3.1")
        self.requires("openal-soft/1.22.2")
    
    def build_requirements(self):
        self.tool_requires("ninja/1.11.1")
        self.tool_requires("cmake/3.28.1")
        
    