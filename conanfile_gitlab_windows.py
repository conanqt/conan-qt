from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout
from conan.tools.scm import Git

class ConanQt(ConanFile):
    name="conanQt"
    
    settings="os","compiler","build_type","arch"
    generators = "VirtualBuildEnv"
    
    def configure(self):
        self.options["qt"].shared = True
        self.options["qt"].qtdeclarative = True
        self.options["qt"].qttranslations = True
        self.options["qt"].qtimageformats = True
        self.options["qt"].qtlanguageserver = True
        self.options["qt"].qtshadertools = True
        self.options["qt"].qtsvg = True

    def requirements(self):
        self.requires("qt/6.5.3@conanqt+conan-qt/stable")
        self.requires("brotli/1.1.0@conanqt+conan-qt/stable")
        self.requires("bzip2/1.0.8@conanqt+conan-qt/stable")
        self.requires("double-conversion/3.3.0@conanqt+conan-qt/stable")
        self.requires("freetype/2.13.2@conanqt+conan-qt/stable")
        self.requires("glib/2.78.1@conanqt+conan-qt/stable")
        self.requires("harfbuzz/8.3.0@conanqt+conan-qt/stable")
        self.requires("libffi/3.4.4@conanqt+conan-qt/stable")
        self.requires("libgettext/0.22@conanqt+conan-qt/stable")
        self.requires("libiconv/1.17@conanqt+conan-qt/stable")
        self.requires("libpng/1.6.42@conanqt+conan-qt/stable")
        self.requires("libpq/15.4@conanqt+conan-qt/stable")
        self.requires("md4c/0.4.8@conanqt+conan-qt/stable")
        self.requires("openssl/3.2.1@conanqt+conan-qt/stable")
        self.requires("pcre2/10.42@conanqt+conan-qt/stable")
        self.requires("sqlite3/3.45.0@conanqt+conan-qt/stable")
        self.requires("zlib/1.3.1@conanqt+conan-qt/stable")
        self.requires("openal-soft/1.22.2@conanqt+conan-qt/stable")
        
    def build_requirements(self):
        self.tool_requires("ninja/1.11.1")
        self.tool_requires("cmake/3.28.1")
        
    