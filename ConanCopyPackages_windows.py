import subprocess

for package in ["brotli/1.1.0", "bzip2/1.0.8", "double-conversion/3.3.0", "freetype/2.13.2", "glib/2.78.1", "harfbuzz/8.3.0",\
                "libffi/3.4.4", "libgettext/0.22", "libiconv/1.17", "libpng/1.6.42", "libpq/15.4", "md4c/0.4.8", "openssl/3.2.1",\
                "pcre2/10.42", "sqlite3/3.45.0", "zlib/1.3.1", "openal-soft/1.22.2", "qt/6.5.3"]:
    subprocess.run(["conan", "copy", "--all", "--force", package, "conanqt+conan-qt/stable"])
