import subprocess

for package in ["qt/6.5.3","libpng/1.6.42","openssl/3.2.1","zlib/1.3.1","xkbcommon/1.5.0", "openal-soft/1.22.2"]:
    subprocess.run(["conan", "copy", "--all", "--force", package, "conanqt+conan-qt/stable"])
